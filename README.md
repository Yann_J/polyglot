# Polyglot

This hipchat addon is based on Hipchat Connect to take advantage of the latest features (side panel UI).
It provides translation services for your chatroom based on Google Translation.

## How to run

* Install npm and redis
* Install npm dependencies
```
npm install
```
* Configure the base URL that the addon will be served from in config.json (must be __https__)

