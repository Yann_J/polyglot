#!/bin/bash

#start redis db
docker run -d -p 6379:6379 -v /opt/polyglot/redis:/data --name redis_polyglot redis

#start the app
node app.js &
