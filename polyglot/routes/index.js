var http = require('request');
var cors = require('cors');
var uuid = require('uuid');
var util = require('util');
var _ = require('lodash');

var DEFAULT_LANGUAGE = 'en';

// This is the heart of your HipChat Connect add-on. For more information,
// take a look at https://developer.atlassian.com/hipchat/tutorials/getting-started-with-atlassian-connect-express-node-js
module.exports = function (app, addon) {
  var hipchat = require('../lib/hipchat')(addon);
  var polyglot = require('../lib/polyglot');

  var getConfig = function(clientInfo,callback) {
    var roomSettings = addon.settings.get('roomSettings',clientInfo.clientKey)
      .then(result => {
        var settings = result || {};
        if(!_.get(settings,'language')) _.set(settings,'language',DEFAULT_LANGUAGE);
        callback(settings);
      })
      .catch(err => {
        callback({language: DEFAULT_LANGUAGE});
      });
  };

  var translateAndPost = function(msg, fromUserLang, toLang, roomId, userId, clientInfo, prefix, showOriginal, callback) {
    addon.settings.get('roomSettings',clientInfo.clientKey)
      .then(settings => {
        addon.settings.get('lang-'+userId,clientInfo.clientKey)
          .then(userLanguage => {
            var fromLang = 'auto';
            var roomLanguage = _.get(settings,'language') || DEFAULT_LANGUAGE;
            userLanguage = userLanguage || 'auto';

            if(toLang == 'user') {toLang = userLanguage;}
            if(toLang == 'room') {toLang = roomLanguage;}

            if(fromUserLang) {fromLang = userLanguage;}

            polyglot.translate(msg, fromLang, toLang, (err,result) => {
              if(result) {
                var response = {
                  languageCode: toLang,
                  translation: result
                };
                var postMessage = _.escape(result);
                if(showOriginal) {
                  postMessage = msg + '<br>' + postMessage;
                }
                if(prefix) {
                  postMessage = `<b>${prefix}:</b> ` + postMessage;
                }

                if(roomId) {
                  hipchat.sendMessage(
                    clientInfo, 
                    roomId, 
                    postMessage,
                    {options: {color: 'purple'}}
                  )
                    .then(function (data) {
                      callback(response);
                    });
                  }
                  else {
                    callback(response);
                  }
              }
              else {
                callback();
              }
            });
          });
      });
  };

  var getGlance = function(settings) {
    return {
      "label": {
        "type": "html",
        "value": "Polyglot"
      },
      "status": {
        "type": "lozenge",
        "value": {
          "label": _.get(settings,'language') || DEFAULT_LANGUAGE,
          "type": "current"
        }
      }
    };
  };

  // simple healthcheck
  app.get(addon.config.contextPath + '/healthcheck', function (req, res) {
    res.send('OK');
  });

  // Root route. This route will serve the `addon.json` unless a homepage URL is
  // specified in `addon.json`.
  app.get(addon.config.contextPath + '/atlassian-connect.json',
    function(req,res) {
      res.json(addon.descriptor);
    }
  );

  app.get(addon.config.contextPath + '/',
    function (req, res) {
      // Use content-type negotiation to choose the best way to respond
      res.format({
        // If the request content-type is text-html, it will decide which to serve up
        'text/html': function () {
          res.render('homepage', addon.descriptor);
        },
        // This logic is here to make sure that the `addon.json` is always
        // served up when requested by the host
        'application/json': function () {
          res.json(addon.descriptor);
        }
      });
    }
  );

  // https://developer.atlassian.com/hipchat/guide/glances
  app.get(addon.config.contextPath + '/glance',
    cors(),
    addon.authenticate(),
    function (req, res) {
      getConfig(req.clientInfo,config => {
        res.json(getGlance(config));
      });
    }
  );

  // https://developer.atlassian.com/hipchat/guide/sidebar
  app.get(addon.config.contextPath + '/sidebar',
    addon.authenticate(),
    function (req, res) {
      // Get room language
      getConfig(req.clientInfo,settings => {
        // Get user's language
        var userId = req.identity.userId;
        addon.settings.get('lang-'+userId,req.clientInfo.clientKey)
          .then(targetLang => {
            // Show panel
            res.render('sidebar', {
              settings: settings || {},
              myLanguage: targetLang || 'auto',
              languages: polyglot.languages || {}
            });
          });
      });
    }
  );

  // https://developer.atlassian.com/hipchat/guide/webhooks
  // This API responds to a webhook on all messages to translate them immediately
  app.post(addon.config.contextPath + '/translate',
    addon.authenticate(),
    function (req, res) {
      translateAndPost(
        _.get(req,'body.item.message.message'),
        false,                 //use user's lang if possible
        'room',               // post in room language
        req.identity.roomId,
        req.identity.userId,
        req.clientInfo,
        _.get(req.body,'item.message.from.name'),
        false,                // don't show original text since it was just posted...
        function() {
          res.sendStatus(200);
        }
      );
    }
  );

  // This route translates an attached message to the room's language
  app.get(addon.config.contextPath + '/translate-to-room',
    addon.authenticate(),
    function (req, res) {

      translateAndPost(
        _.get(req,'query.msg'),
        false, //this does not necessarily come from the user so we need to use auto
        'room', 
        null,  //req.identity.roomId, 
        req.identity.userId, 
        req.clientInfo,
        _.get(req,'query.poster'),
        true, //show original message
        function(result) {
          res.render('translate-dialog',{
            poster       : _.get(req,'query.poster'),
            original     : _.get(req,'query.msg'),
            languageName : polyglot.languages[_.get(result,'languageCode')],
            languageCode : _.get(result,'languageCode'),
            translation  : _.get(result,'translation')
          });
        }
      );
    }
  );

  // This route translates attached message into the requesting user's language
  app.get(addon.config.contextPath + '/translate-to-my',
    addon.authenticate(),
    function (req, res) {
      translateAndPost(
        _.get(req,'query.msg'),
        false, //this does not necessarily come from the user so we need to use auto
        'user',
        null, //req.identity.roomId, 
        req.identity.userId, 
        req.clientInfo,
        _.get(req,'query.poster'),
        true, //show original message
        function(result) {
          res.render('translate-dialog',{
            poster       : _.get(req,'query.poster'),
            original     : _.get(req,'query.msg'),
            languageName : polyglot.languages[_.get(result,'languageCode')],
            languageCode : _.get(result,'languageCode'),
            translation  : _.get(result,'translation')
          });
        }
      );
    }
  );

  // Sets the room language and persist it
  app.post(addon.config.contextPath + '/room-language',
    addon.authenticate(),
    function(req, res) {
      var targetLanguage = _.get(req,'body.language');
      var languageName = _.get(polyglot.languages,targetLanguage);

      // console.log('Setting language to '+targetLanguage);

      getConfig(req.clientInfo,settings => {
        // console.log(util.inspect(settings));
        if(_.isEqual(targetLanguage,_.get(settings,'language'))) {
          // Same language, ignore...
          console.log('Ignoring new language');
          res.sendStatus(200);
        }
        else {
          _.set(settings,'language',targetLanguage);
          // console.log(util.inspect(settings));
          addon.settings.set('roomSettings',settings,req.clientInfo.clientKey)
            .then(result => {
              // Reply to client
              res.sendStatus(200);

              // Notify room
              hipchat.sendMessage(
                req.clientInfo,
                req.identity.roomId,
                `(successful) New room language: ${languageName} (${targetLanguage})`,
                {options: {color: 'yellow', format:'text'}}
              );

              // Update glance UI
              hipchat.updateGlance(req.clientInfo, req.identity.roomId, "polyglot.glance", getGlance(settings));
            });
        }
      });
    }
  );

  // Sets the user's language and persist it
  app.post(addon.config.contextPath + '/my-language',
    addon.authenticate(),
    function(req, res) {
      var targetLanguage = _.get(req,'body.language');
      var userId = req.identity.userId;

      // console.log('Setting language to '+targetLanguage);

      addon.settings.set('lang-'+userId,targetLanguage,req.clientInfo.clientKey)
        .then(result => {
          // Reply to client
          res.sendStatus(200);
        });
    }
  );

  // Notify the room that the add-on was installed. To learn more about
  // Connect's install flow, check out:
  // https://developer.atlassian.com/hipchat/guide/installation-flow
  addon.on('installed', function (clientKey, clientInfo, req) {
    var currentLang = _.get(polyglot.languages, DEFAULT_LANGUAGE) || 'English';
    hipchat.sendMessage(clientInfo, req.body.roomId, 'Polyglot has been installed in this room. You can now speak hundreds of languages! Configure your language preferences in the right-side panel. The current room language is '+currentLang);
  });

  // Clean up clients when uninstalled
  addon.on('uninstalled', function (id) {
    addon.settings.client.keys(id + ':*', function (err, rep) {
      rep.forEach(function (k) {
        addon.logger.info('Removing key:', k);
        addon.settings.client.del(k);
      });
    });
  });

};
