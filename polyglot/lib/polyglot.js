var translate = require('google-translate-api');
var _ = require('lodash');
// var async = require('async');

module.exports = {
	translate: function(text, fromLanguage, toLanguage, callback) {
		console.log(`Translating ${text} from ${fromLanguage} to ${toLanguage}...`);
		if(toLanguage && _.isEqual(fromLanguage,toLanguage)) {
			console.log('Source and destination languages are the same, ignoring...');
			return callback(new Error('SAME_LANGUAGE'),null);
		}

		translate(text, {
			from: fromLanguage || 'auto',
			to: toLanguage || 'en',
		})
		.then(result => {
			console.log(`Result: ${result.text}`);

			// Check if the detected language is same as target
			var detectedLanguage = _.get(result,'from.language.iso');
			if('auto'==fromLanguage && _.isEqual(detectedLanguage, toLanguage)) {
				console.log('Detected source and destination languages are the same, ignoring...');
				callback(new Error('SAME_LANGUAGE'),null);
			}
			else {
				callback(null,result.text);
			}
		})
		.catch(err => {
			console.error(`Error submitting translation: ${err}`);
			callback(err,null);
		});
	},

	languages: _.omit(translate.languages,['isSupported','getCode','auto']),
};
